`timescale 1ns/1ns

module measure_period(
    input clk,
    output time period
);
time last_time;

always@(posedge clk) begin
    period = $time - last_time;
    last_time = $time;
end

endmodule