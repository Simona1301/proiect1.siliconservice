`timescale 1ns/1ps 
module pwm#
( parameter N = 3,
  parameter nr = 8
) 
   ( input cmdval,
     input cmdRW,
     input [1:0] addr,
     input [15:0] data_in,
     input clk,
     input reset,
     output reg [15:0] data_out,
     output reg pwm_out
    // output  new_clk;
    );
 
   reg [15:0] 	period = 16'b0;
   reg [15:0] 	dc  = 16'b0;
   reg [2:0] 	prescaler = 3'b0;
   reg 		ctrl = 1'b0;

   reg [N-1:0] counter_p = 0; // register for implementing the N bit counter for prescaler
   wire 	       new_clk;
   wire 	       DC;
   reg [nr-1:0] count = 0;
   
 always@(posedge clk)
   begin
      if(reset)
	counter_p <= 1'b0;
      else
	 counter_p <= counter_p + 1;
   end   
 assign new_clk = counter_p[N-1]; // prescaler
 //assign prescaler = counter_p[N-1];

always@(posedge new_clk)
  begin
     if (reset)
       count <= 1'b0;
     else if(count < period)
       count <= count + 1;
     else
       count <= 0;
  end
assign DC = (count < dc) ? 'b1:'b0; // duty_cycle

   always @(posedge clk)
     begin
	if(reset)
	  begin
	     period <= 0;
	     dc <= 0;
	     prescaler <= 0;
	     ctrl <= 0;
	  end
	else begin
	   // if(ctrl == 1) //start
	   // begin
	   if(cmdval == 1) // valid command 
	     begin
		if(cmdRW == 0) // write
		  begin
		     case(addr)
		       2'b00: period = data_in;
		       2'b01: dc = data_in;
		       2'b10: prescaler = data_in;
		       2'b11: ctrl = data_in;
		       //default: data_in <= 16'bx;
		       //default: data_in <= data_in;
		       default: $display("default write");
		     endcase 
		  end
		else //if(cmdRW == 1) // read
		begin
		   case(addr) 
		     2'b00: data_out = period;
		     2'b01: data_out = dc;
		     2'b10: data_out = prescaler;
		     2'b11: data_out = ctrl;
		     default: data_out = 16'bx;
		      // default: data_out <= data_out;
		   endcase
		end
		// pwm_out <= DC;
	     end // if (cmdval == 1)
	   //end
	   //else 
	   // pwm_out <= 1'bz;
	   
	   if(ctrl == 1'b0) //stop
	     pwm_out <= 'bz;
	   else if (ctrl == 1'b1) //start
	     pwm_out <= DC;
	 
	end
     end // always @ (posedge clk)   
         
endmodule