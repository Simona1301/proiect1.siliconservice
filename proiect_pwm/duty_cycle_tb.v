`timescale 1ns/1ps 
module duty_cycle_tb();

  parameter N = 50;
  parameter nr = 8;
  parameter dc = 20;
   
  reg clk = 0;
  wire DC;

   duty_cycle #(.N(N), .dc(dc), .nr(nr))
   duty_cycle (.clk(clk), .DC(DC));
   always #1 clk = ~clk;

   initial begin
       # 500 $finish;
   end
     
   endmodule
   