`timescale 1ns/1ns

module period_tb();

reg clk;
wire [63:0] period;

measure_period m_p(.clk(clk),.period(period));

always #3 clk = ~clk;

initial begin
    $monitor($realtime, period);
    clk = 0;
    #500 $finish;
end

endmodule