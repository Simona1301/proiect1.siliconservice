`timescale 1ns/1ps 
module pwm_tb();
   parameter N = 3;
   parameter nr = 8;

   reg cmdval; // = 0;
   reg cmdRW; // = 0;
   reg [1:0] addr; // = 0;
   reg [15:0] data_in; // = 0;
   reg 	      clk; // = 0;
   reg 	      reset; // = 0;
   
   wire [15:0] data_out;
   wire pwm_out;
   wire new_clk;
   

   pwm# (.N(N), .nr(nr))
   pwm_1(.cmdval(cmdval), 
	 .cmdRW(cmdRW), 
	 .addr(addr), 
	 .data_in(data_in), 
	 .clk(clk), 
	 .reset(reset), 
	 .data_out(data_out), 
	 .pwm_out(pwm_out));
   
	// .new_clk(new_clk));

   always #5 clk = ~clk;

   initial begin
      cmdval =1'b0;
      cmdRW = 1'b0;
      addr = 2'b00;
      data_in = 16'b0;
      clk = 1'b0;
      reset = 1'b0;
      
      cmdval = 1'b1;
      
      #6 reset = 1'b1;
      #1 reset = 1'b0;
      
      cmdRW = 1'b0;
      #1 addr = 2'b11;
      data_in = 16'b1;

       #10 cmdRW = 1;
      addr = 2'b11;

      #2 cmdRW = 0;
      
     #2 addr = 2'b00;
      data_in = 16'b101;
      
      #3 addr = 2'b10;
      data_in = 16'b111;
    
     #5 addr = 2'b01;
      data_in = 16'b10;
      
      #5 addr = 2'b01;
      data_in = 16'b11;

       #5 addr = 2'b01;
      data_in = 16'b101;
      
     // #30 reset = 1;
     // #1 reset = 0;
      
     #10 cmdRW = 1;
      addr = 2'b11;
     // data_in = 16'b1;
     
 #500 cmdval = 0;
      
    
      #300 $finish;
   end
   endmodule
      
      
      
   
   
   