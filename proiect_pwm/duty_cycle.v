`timescale 1ns/1ps 
module duty_cycle#
   (
   parameter N = 50,//t
   parameter nr = 8,
   parameter dc = 20  )
 (
    input clk, 
    output DC );
   
reg [nr-1:0] count = 0;

always@(posedge clk)
	begin
		if(count < N) count <= count + 1;
		else count <= 0;
	end

assign DC = (count < dc) ? 1:0;

endmodule