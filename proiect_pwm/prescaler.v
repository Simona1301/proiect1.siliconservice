`timescale 1ns/1ps 
module prescaler#
   (
   parameter N = 3 )// number of bits of the prescaler
   (
    input clk_in, 
    output clk_out );
   
reg [N-1:0] count = 0; // register for implementing the N bit counter
    
assign clk_out = count[N-1]; // the most significant bit goes through the output
    
always @(posedge(clk_in))
  begin
     count <= count + 1;
  end
    
endmodule