`timescale 1ns/1ps 
module prescaler_tb();

parameter N = 3; // number of bits of the tested prescaler
reg clk = 0; 
wire clk_out;

prescaler #(.N(N)) // instantiate the N-bit prescaler
  Pres1(
    .clk_in(clk),
    .clk_out(clk_out)
  );
 
always #1 clk = ~clk;

initial begin  
 // $dumpfile("prescaler_tb.vcd");
 // $dumpvars(0, prescaler_tb);
    
  # 99 $display("END of simulation");
  # 100 $finish;
end
endmodule