#pragma once

#define max_ex 4

#define p_clk 500
#define p_th p_clk/2
#define d_IC 3
#define d_DE 2
#define d_EX 5
#define d_LS 6

#define MASK_OPC 0xA & 0x3F
#define MASK_SRC1 0x5 & 0x1F
#define MASK_SRC2 0x1F

//registri
#define  R0 0x1
#define  R1 0x2
#define  R2 0x3
#define  R3 0x4
#define  R4 0x5
#define  R5 0x6
#define  R6 0x7
#define  R7 0x8

#define IMM 0x10
#define ADDR 0x11
#define ADDR_R 0x12

//FLAGS
#define Z 0
#define E 1
#define G 2

//instructiuni
#define ADD 0x1
#define SUB 0x2
#define MOV 0x3
#define MUL 0x4
#define DIV 0x5
#define CMP 0x6
#define JMP 0x7
#define JE 0x9
#define JL 0xA
#define JG 0xB
#define JZ 0xC
#define CALL 0xD
#define RET 0xE
#define END_SIM 0xF
#define PUSH 0x10
#define POP 0x11


union window {
	uint64_t val64;
	uint16_t val16[4];
};

struct packet {
	window window;
	uint16_t IP;
};

/*union window_n {
	uint64_t val64;
	uint16_t val16[4];
};

struct packet {
	window_n window_n;
	uint16_t IP;
};*/

struct instr {
	struct header {
		uint16_t opc : 6;
		uint16_t src1 : 5;
		uint16_t src2 : 5;
	};
	header header;
	
	uint16_t p1;
	uint16_t p2;

	uint16_t dest; // acelasi cu src1

	uint16_t ip_i;
	uint16_t size; // dimensiunea instructiunii
	uint16_t value;
	uint16_t addr;
	uint16_t addr_n;
	
};



