#pragma once
#include <mutex>
#include <string>
#include<queue>
#include "memory.h"

using namespace std;

class logger {
public:

	void print_ip(string caller, uint16_t message);
	void print(string caller, uint64_t message, uint16_t ip);
	void print_instr_DE( struct instr& instruction);
	void print_instr_EX(struct instr& instruction, uint16_t IP);
	void print_m(string message);
private:
	mutex mt;
};

class cpu	{
	
	memory *mem;
	mutex mt;

	//internal registers
	uint16_t R[8]; // general registers
	uint16_t FLAGS[3]; // state register
	uint16_t IP; //instruction pointer
	
	//config registers
	uint16_t stack_base;
	uint16_t stack_size;
	uint16_t mem_base;
	uint16_t mem_size;
	uint16_t sp;

	logger *ptrLog;
	bool run;
	
public:
	//cpu(memory *mem)	{	this->mem = mem;	}	
	//cpu(logger *_ptr)	{	ptrLog = _ptr;	}

	cpu(logger *_ptr, memory *mem){ ptrLog = _ptr; this->mem = mem;}
	uint64_t curCycles();
	void store(uint16_t address, uint64_t data);
	uint16_t load(uint16_t address);
	void instructiune_ilegala();
	void clk();
	void start_sim();
	void stop_sim();
	void update(uint16_t valoare, uint16_t dest);

	void IC();
	void DE();
	void EX();
	void LS();
	void CompletionBuffer();
	
};


/*template<class T>
class SafeQueue {

	queue<T> q;
	mutex mt;

public:

	SafeQueue() {}

	void push_t(T elem) {

		mt.lock();
		q.push(elem);
		mt.unlock();

	}

	T pop_t() {

		mt.lock();
		T elem = q.front();
		q.pop();
		mt.unlock();

		return elem;
	}

	size_t size()
	{
		lock_guard<std::mutex> lock(mt);
		return q.size();
	}

	T front() 
	{
		lock_guard<std::mutex> lock(mt);
		return q.front();
	}

	T back()
	{
		lock_guard<std::mutex> lock(mt);
		return q.back();
	}

	T empty()
	{
		lock_guard<std::mutex> lock(mt);
		return q.empty();
	}
		
};*/
