#include <iostream>
#include <fstream>
#include <string>
#include "memory.h"
#include "define.h"

memory::memory(string file_name)
{
	ifstream f;
	f.open("input.txt");
	string line;
			
	if (!f)
		cout << "eroare la deschiderea fisierului" << endl;
	else
	{
		cout << "structura memeoriei este: [address] -> data" << endl;
		while (!f.eof())
		{
			getline(f, line);
			cout << line << endl;

			if (line[0] == '#') // daca primul caracter de pe linia respectiva este #
			{
				line.erase(0, 1);   // vreau sa il sterg

				address = stoi(line, nullptr, 16); //	ce a ramas convertesc in int
				//address = stoi(const std::string& str, std::size_t* pos = 0, int base = 10);
			}
			else
			{
				data = stoi(line, nullptr, 16); // convertesc in int 
				mem[address] = data;
				address = address + 2;
			}
		}
	}

	f.close();
}

void memory::print_memory()
{
	//map<uint16_t, uint16_t> ::iterator it;

	cout << "continutul memoriei este: [adresa] -> data" << endl;

	for (auto it = mem.begin(); it != mem.end(); it++)
	{
		cout <<hex<< it->first << " " << it->second << " "<< "\n";
	}
	cout << "Memoria este printata!" << endl;

}

/*//vechea functie read_memory
uint64_t memory::read_memory(uint16_t address)
{
	//return mem[address];

	int i;
	uint64_t data = 0;
	uint16_t temp;
	//uint16_t naddress = address;// am incercat cu address dar am eroare, asa ca am folosit alta variabila
	//uint16_t address;


	map<uint16_t, uint16_t>::iterator it;
	
	for ( i = 3; i >= 0; i--)
	{
		it = mem.find(address + 2 * i); // caut in sir
		
		if (it != mem.end())
			{
				temp = mem[address+2*i];
			}
		else
			{
			temp = 0xFFFF;
			}
		
	//	naddress = naddress + 2;
		data = (data <<16) | temp; //shift cu 16*i
	}
	//cout << endl << "am citit" << endl;
	return data;
	}
*/

void memory::write_memory(uint16_t address, uint16_t data)
{
	mem[address] = data;
}

void memory::print_file(string file_name)
{
		ofstream g(file_name);

		g << "continutul memoriei este: [adresa] -> data" << endl;

		for (auto it = mem.begin(); it != mem.end(); it++)
		{
		g << hex << it->first << " " << it->second << " " << "\n";
		}
		g << "Memoria este printata!" << endl;
	
	g.close();
}
