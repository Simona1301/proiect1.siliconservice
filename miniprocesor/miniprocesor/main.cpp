#include"memory.h"
#include <iostream>
#include"cpu.h"
#include <thread>
//#include <fstream>
//#include <string>

using namespace std;

int main()
{
	//***************memory part*****************
	memory mem("input.txt");
	mem.print_memory();

   // uint16_t address = 0xfff0;
	//cout << endl << "citire" << endl << mem.read_memory<uint64_t>(address) << endl;
	
	//mem.write_memory(address, data);
	mem.print_file("testare.txt");
			
	//****************cpu part******************		
	logger vasile;
	cpu cpu(&vasile, &mem);
	
	cpu.start_sim();
	cpu.stop_sim();

	system("pause");
			
	return 0;
}