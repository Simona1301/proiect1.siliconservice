#pragma once
#include <map>
using namespace std;

class memory {

	uint16_t address;
	uint16_t data;
	map <uint16_t, uint16_t> mem;

public:
	memory (string file_name = "" ); //constructor
	void print_memory ();
	//uint64_t read_memory (uint16_t address);
	void write_memory(uint16_t address, uint16_t data);
	void print_file(string );

template<typename T> T read_memory(uint16_t address) {
		T data = 0;
		int size = sizeof(data) / sizeof(uint16_t);
		//cout << "Size is " << size << endl;
		uint16_t temp = 0;

		for (int i = size-1; i >= 0; i--)
		{
			if (mem.count(address + 2 * i) == 0) { //nothing found at addr
				temp = 0xffff;
			}
			else { //found smthing in memory
				temp = mem[address + 2 * i];
			}
			data = data << 16 | temp;
			//cout << "Current iter " << i << " addr:"<<address+2*i<<" data: " << data << endl;
		}
		return data;

	}

};