#include <iostream>
#include "cpu.h"
#include "memory.h"
#include "define.h"
#include <thread>
#include <chrono>
#include <mutex>
#include <string>
#include<queue>
#include <vector> 

using namespace std;

uint64_t cycle = 0;
instr instruction;
uint16_t ip;
uint64_t ip8;
bool newIP = true;
packet p;

queue <uint64_t>ICtoLS; 
queue <packet>LStoIC;
queue <packet>ICtoDE;
queue <uint64_t>DEtoIC;
queue <uint64_t>DEtoLS;
queue <uint64_t>LStoDE;
queue <instr>DEtoEX; 
//queue <uint64_t>EXtoLS;
//queue <uint64_t>EXtoIC;
queue <uint64_t>EXtoCB;
queue <uint64_t>CBtoLS;

void cpu::clk(){
	while (1) {
		this_thread::sleep_for(chrono::milliseconds(p_clk));
		cycle++;
	}
}

uint64_t cpu::curCycles() {
	return cycle;
}

void cpu::start_sim(){
	//IP = 0xFFF0;
	IP = 0x0000;
	run = true;

	thread t_clk(&cpu::clk, this);
	thread t_ic(&cpu::IC, this);
	thread t_de(&cpu::DE, this);
	thread t_ex(&cpu::EX, this);
	thread t_ls(&cpu::LS, this);
	thread t_CompletionBuffer(&cpu::CompletionBuffer, this);
	
	std::vector<std::thread> threads;
	for (int i = 0; i <= max_ex; i++)
		threads.push_back(thread (&cpu::EX, this));
	cout << "Synchronizing all threads...\n";
	for (auto& th : threads) th.detach();

	t_clk.detach();
	t_ic.detach();
	t_de.detach();
	t_ex.detach();
	t_ls.detach();
	t_CompletionBuffer.detach();
}

void cpu::stop_sim() {
	while (cycle < 100) {
		this_thread::sleep_for(chrono::milliseconds(p_clk));
	}
	run = false;
}

uint16_t cpu::load(uint16_t address) {
	uint16_t data = 0;
	DEtoLS.push(address);
	
	while (data == 0){
		if (LStoDE.size() != 0)
		{			
			data = static_cast<uint16_t>(LStoDE.front());
			LStoDE.pop();
			//data = LStoDE.pop_t();
			return data;
		}
		else
			this_thread::sleep_for(chrono::milliseconds(p_th));
	}
}

void cpu::store(uint16_t address, uint64_t data){
	 mem->write_memory(address, data);
}

void cpu::instructiune_ilegala(){
	string message = "Illegal instruction!";
	ptrLog->print_m(message);
}

void cpu::IC() {
	packet IC_data;

	while (run) {
		uint64_t curentCycle = cycle;
		while (cycle < curentCycle + d_IC) {
			this_thread::sleep_for(chrono::milliseconds(p_th));
		}

		while (newIP == true)
		{
			if (ICtoLS.empty() && LStoIC.empty())
			{
				ip = IP & ~7;
				ICtoLS.push(IP);
				string caller1 = "IC sends IP to LS ";
				ptrLog->print_ip(caller1, ip);
			
				newIP = false;
			}
		}

		if (!DEtoIC.empty())
		{
			ip8 = DEtoIC.front();
			DEtoIC.pop();
			ip8 = ip8 & ~7;
			ICtoLS.push(ip8);
			//ip = ip + 2;
			string caller2 = "IC sends IP8 to LS ";
			ptrLog->print_ip(caller2, ip8);
		}

		if (!LStoIC.empty())
		{
			IC_data = LStoIC.front();
			LStoIC.pop();
			string caller_r = "IC received data from LS ";
			ptrLog->print(caller_r, IC_data.window.val64, p.IP);

			ICtoDE.push(IC_data);  //w.val64
			string caller_s = "IC sends data to DE ";
			ptrLog->print(caller_s, IC_data.window.val64, p.IP);
		}
	}	
}
void cpu::DE() {
	bool instrInCompleta = false;
	bool done = false;
	int i= 0;
	//uint16_t previous_instr = 0;
	packet DE_data;
	packet DE_new;
	//DE_data.IP = 0;
	uint16_t data = 0;
	while (run) 
	{
			if (!ICtoDE.empty())
			{
				DE_data = ICtoDE.front();
				ICtoDE.pop();
				string caller = "DE received data from IC ";
				ptrLog->print(caller, DE_data.window.val16[i], p.IP);
				bool p1_valid = false;
				bool p2_valid = false;
				//instrInCompleta = false;
			
				//LOOP: while (done = false)
				//{
					while ((IP >= DE_data.IP) && (IP < DE_data.IP + 8))
					{
						i = (IP >> 1) % 4;
						// sau i = (IP >> 1) & 3;
						cout << endl << "ip is:" << p.IP << endl;
						cout << endl << "data is:" << p.window.val16[i] << endl;

						instruction.header.src2 = p.window.val16[i] & MASK_SRC2;
						instruction.header.src1 = p.window.val16[i] >> MASK_SRC1;
						instruction.header.opc = p.window.val16[i] >> MASK_OPC;
						instruction.ip_i = IP;
						i++;

						if (instruction.header.opc != 0)
						{
							instruction.size = 1; // 16 biti
						}
						if (instruction.header.src1 == IMM || instruction.header.src1 == ADDR || instruction.header.src1 == ADDR_R)
						{
							instruction.size++; // 32 biti
							p1_valid = true;
						}
						if (instruction.header.src2 == IMM || instruction.header.src2 == ADDR || instruction.header.src2 == ADDR_R)
						{
							instruction.size++; // 48 biti
							p2_valid = true;
						}

						if (i + instruction.size > 4)
						{
							instrInCompleta = true;
							string message1 = "Incomplete instruction!";
							ptrLog->print_m(message1);
							DEtoIC.push(ip + 8);

							string message2 = "DE requesting a new window from IC ";
							ptrLog->print_ip(message2, ip);

							if (ICtoDE.empty())
							{
								this_thread::sleep_for(chrono::milliseconds(p_th));
							}
							else
							{
								//w_n.val64 = ICtoDE.front();
								//ICtoDE.pop();
								//w.val16[i]= ICtoDE.pop_t();
								DE_new = DE_data;
								DE_new = ICtoDE.front();
								ICtoDE.pop();
								string caller = "DE received new window from IC ";
								ptrLog->print(caller, DE_new.window.val64, p.IP);

							}

							//fac salvarea
							if (instruction.header.src1 == ADDR) //doar daca e address face load 
							{
								if (ICtoDE.empty())
								{
									/*w.val16[i] = copy_val16[i];
									instruction.p1 = copy_val16[i];
									i++;*/
									DE_new = DE_data;
									//instruction.p1 = DE_new.window.val16[i];
									instruction.p1 = load(data);
									i++;
								}
							}
							else if (instruction.header.src2 == ADDR)
							{
								if (p1_valid == 1)
								{
									if (i == 4)
									{
										if (ICtoDE.empty())
										{
											/*w.val16[i] = copy_val16[i];
											instruction.p1 = copy_val16[i];
											i++;*/
											DE_new = DE_data;
											//instruction.p1 = DE_new.window.val16[i];
											instruction.p1 = load(data);
											i++;
										}
									}
								}

								if (p2_valid == 1)
								{
									if (i == 5)
									{
										/*w.val16[i] = copy_val16[i];
										instruction.p2 = copy_val16[i];*/
										DE_new = DE_data;
										//instruction.p2 = DE_new.window.val16[i];
										instruction.p2 = load(data);
									}
									else
									{
										//instruction.p2 = DE_data.window.val16[i];
										instruction.p2 = load(data);
										i++;
									}
								}
							}	i++;
							instrInCompleta = false;
						}
	
					switch (instruction.header.opc)
					{
					case ADD:
					case SUB:
					case MOV:
						if (instruction.header.src1 == IMM)	{	instructiune_ilegala();	}
						else {
							DEtoEX.push(instruction);
							ptrLog->print_instr_DE(instruction);
						}
					break;

					case DIV:
						if (instruction.header.src2 == 0)	{	instructiune_ilegala();	}
						else {
							DEtoEX.push(instruction);
							ptrLog->print_instr_DE(instruction);
						}
					break;

					case MUL:
					case CMP:
					case JMP:
					case JE:
					case JL:
					case JG:
					case JZ:
					case CALL:
					case RET:
					case END_SIM:
					case POP:
					case PUSH:
					{
						DEtoEX.push(instruction);
						ptrLog->print_instr_DE(instruction);
					}
					break;

					default:
					{
						string message = "DEFAULT (DE)!";
						ptrLog->print_m(message);
						p.window.val16[i] = 0x0;
					}
					}
					if (p.window.val16[i] == 0x0 && i < 3)
					{
						p.window.val16[i] = p.window.val16[i + 1];
						DEtoEX.push(instruction);
						ptrLog->print_instr_DE(instruction);
					}
	
					uint64_t curentCycle = cycle;
					while (cycle < curentCycle + d_DE) {
						this_thread::sleep_for(chrono::milliseconds(p_th));
					}
					//done = true;
					//goto LOOP;
				}	
				cout << endl << "HERE ARE " << DEtoEX.size() << " INSTRUCTIONS!" << endl;
			} 
	}
}


void cpu::EX() {
	uint16_t src1;
	uint16_t src2;
	uint16_t result_instr;
	uint32_t mul_result;
	//bool done = false;
	uint16_t ip = IP;
	int i = 0;
	while (run) {
				
		uint64_t curentCycle = cycle;
		while (cycle < curentCycle + d_EX) {
			this_thread::sleep_for(chrono::milliseconds(p_th));
		}

		/*for (int i = 0; i <= max_ex; i++)
		{
			i++;
			cout << endl << "EX" << i << " execute!" << endl;
		}*/

		if ( DEtoEX.size() != 0) 
		{
			while (i <= max_ex)//-----------------------
			{

				instruction = DEtoEX.front();
				DEtoEX.pop();
				//instruction = DEtoEX.pop_t();
				ptrLog->print_instr_EX(instruction, IP);

				//i++;
				cout << endl << "EX" << i << " execute!" << endl;

				switch (instruction.header.opc)
				{
				case ADD:
					cout << endl << " ADD instruction" << endl;
					if (instruction.header.src1 >= R0 && instruction.header.src1 <= R7)
					{
						src1 = instruction.header.src1;
						cout << " dest/src1 is: R[" << src1 << "]" << endl;

						// register+register
						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							src2 = instruction.header.src2;
							cout << " src2 is: R[" << src2 << "]" << endl;

							instruction.dest = src1;
							R[instruction.dest] = R[instruction.dest] + R[src2];
							result_instr = R[instruction.dest];
							cout << "the result of the instruction is: R[" << result_instr << "]" << endl;

							if (result_instr == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						//  register + ADDR
						else if (instruction.header.src2 == ADDR)
						{
							cout << endl << "add instruction between a register and ADDR!" << endl;
							src2 = instruction.addr;
							instruction.dest = src1;
							//store(R[src1], load(R[src1]) + load(IP));


							if (result_instr == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						// register + IMM
						else if (instruction.header.src2 == IMM)
						{
							cout << endl << "add instruction between a register and IMM!" << endl;
							instruction.dest = src1;
							R[instruction.dest] = R[instruction.dest] + instruction.value;
							result_instr = R[instruction.dest];
							cout << "the result of the instruction is: R[" << result_instr << "]" << endl;

							if (result_instr == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						// register + ADDR_R
						else if (instruction.header.src2 == ADDR_R)
						{
							//------------------
							cout << endl << "add instruction between a register and ADDR_R!" << endl;
						}
						//IP = IP + 2 * instruction.size;
					}

					else if (instruction.header.src1 == ADDR)
					{
						src1 = instruction.addr;
						cout << " dest/src1 is: [" << src1 << "]" << endl;

						//ADDR + register
						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							cout << endl << "add instruction between a ADDR and register!" << endl;
							src2 = instruction.header.src2;
							cout << " src2 is: R[" << src2 << "]" << endl;
							instruction.dest = src1;

							//store(src1, load(src1) + R[src2]);
						}
						else if (instruction.header.src2 == ADDR)
						{
							cout << endl << "add instruction between a ADDR and ADDR!" << endl;
							src2 = instruction.addr;
							cout << " src2 is: [" << src2 << "]" << endl;
							instruction.dest = src1;

							//store(src1, load(src1) + load(src2));
						}
						else if (instruction.header.src2 == IMM)
						{
							cout << endl << "add instruction between a ADDR and IMM!" << endl;
							//store(src1, load(src1) + load(instruction.value));
						}
						else if (instruction.header.src2 == ADDR_R)
						{
							//---------------
							cout << endl << "add instruction between a ADDR and ADDR_R!" << endl;
						}
						//IP = IP + 2 * instruction.size;
					}
					break;

				case SUB:
					cout << endl << " SUB instruction" << endl;
					// register-register
					if (instruction.header.src1 >= R0 && instruction.header.src1 <= R7)
					{
						src1 = instruction.header.src1;

						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							src2 = instruction.header.src2;

							instruction.dest = src1;
							R[instruction.dest] = R[instruction.dest] - R[src2];
							result_instr = R[instruction.dest];
							cout << endl << "the result of the instruction is: " << result_instr << endl;

							if (result_instr == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						//  register - ADDR
						else if (instruction.header.src2 == ADDR)
						{
							cout << endl << "sub instruction between a register and ADDR!" << endl;
							src2 = instruction.addr;
							instruction.dest = src1;
							store(R[src1], load(R[src1]) - load(IP));

							if (result_instr == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						// register - IMM
						else if (instruction.header.src2 == IMM)
						{
							cout << endl << "sub instruction between a register and IMM!" << endl;
							instruction.dest = src1;
							R[instruction.dest] = R[instruction.dest] - instruction.value;
							result_instr = R[instruction.dest];
							cout << endl << "the result of the instruction is: R[" << result_instr << "]" << endl;

							if (result_instr == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						// register - ADDR_R
						else if (instruction.header.src2 == ADDR_R)
						{
							//------------------
							cout << endl << "sub instruction between a register and ADDR_R!" << endl;
						}
						//IP = IP + 2 * instruction.size;
					}

					else if (instruction.header.src1 == ADDR)
					{
						src1 = instruction.addr;
						cout << " dest/src1 is: [" << src1 << "]" << endl;

						//ADDR - register
						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							cout << endl << "sub instruction between a ADDR and register!" << endl;
							src2 = instruction.header.src2;
							cout << " src2 is: R[" << src2 << "]" << endl;
							instruction.dest = src1;
							//store(src1, load(src1) - R[src2]);
						}
						else if (instruction.header.src2 == ADDR)
						{
							cout << endl << "sub instruction between a ADDR and ADDR!" << endl;
							src2 = instruction.addr;
							cout << " src2 is: [" << src2 << "]" << endl;
							instruction.dest = src1;

							//store(src1, load(src1) - load(src2));
						}
						else if (instruction.header.src2 == IMM)
						{
							cout << endl << "sub instruction between a ADDR and IMM!" << endl;
							//store(src1, load(src1) - load(instruction.value));
						}
						else if (instruction.header.src2 == ADDR_R)
						{
							//---------------
							cout << endl << "sub instruction between a ADDR and ADDR_R!" << endl;
						}
						//IP = IP + 2 * instruction.size;
					}
					break;

				case MOV:
					cout << endl << " MOV instruction" << endl;
					//instruction.dest = instruction.header.src1;

					if (instruction.header.src1 >= R0 && instruction.header.src1 <= R7)
					{
						src1 = instruction.header.src1;
						cout << " dest/src1 is: R[" << src1 << "]" << endl;

						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							src2 = instruction.header.src2;
							cout << " src2 is: R[" << src2 << "]" << endl;
							instruction.dest = src1;
							R[instruction.dest] = instruction.dest;
							R[src2] = src2;
							R[instruction.dest] = R[src2];
							result_instr = R[instruction.dest];
							cout << "the result of the instruction is: R[" << result_instr << "]" << endl;
						}
						else if (instruction.header.src2 == ADDR)
						{
							cout << endl << "MOV instruction between a register and ADDR!" << endl;
							src2 = instruction.addr;
							cout << " src2 is: [" << src2 << "]" << endl;
							instruction.dest = src1;
							//R[src1] = load(src2);
							cout << "the result of the instruction is: R[" << src1 << "]" << endl;
						}
						else if (instruction.header.src2 == IMM)
						{
							cout << endl << "MOV instruction between a register and IMM!" << endl;
							instruction.dest = src1;
							R[instruction.dest] = instruction.value;
							result_instr = R[instruction.dest];
							cout << "the result of the instruction is: R[" << result_instr << "]" << endl;
						}
					}
					else if (instruction.header.src1 == ADDR)
					{
						//src1 = instruction.addr;
						src1 = instruction.header.src1;
						cout << " dest/src1 is: [" << src1 << "]" << endl;

						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							cout << endl << "MOV instruction between a ADDR and register!" << endl;
							src2 = instruction.header.src2;
							cout << " src2 is: R[" << src2 << "]" << endl;
							instruction.dest = src1;
							//store(src1, R[src2]);

						}
						else if (instruction.header.src2 == ADDR)
						{
							cout << endl << "MOV instruction between a ADDR and ADDR!" << endl;
							//src2 = instruction.addr_n;
							src2 = instruction.header.src2;
							cout << " src2 is: [" << src2 << "]" << endl;
							instruction.dest = src1;
							//store(src1, load(src2));
							cout << "am reusit" << endl;
							//IP = IP + 2;
						}
						else if (instruction.header.src2 == IMM)
						{
							cout << endl << "MOV instruction between a ADDR and IMM!" << endl;
							//store(src1, load(instruction.value));
						}
					}
					break;

				case MUL:
					cout << endl << " MUL instruction" << endl;
					if (instruction.header.src1 >= R0 && instruction.header.src1 <= R7)
					{
						src1 = R[instruction.header.src1];
						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							src2 = R[instruction.header.src2];

							R[0] = 0;
							R[1] = src1 * src2;
							mul_result = R[1];
							cout << endl << "R[0] = " << R[0] << " R[1] = " << R[1] << " mul_result" << mul_result << endl;

							if (mul_result == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}	//IP = IP + 2;
					}

					break;

				case DIV:
					cout << endl << " DIV instruction" << endl;
					if (instruction.header.src1 >= R0 && instruction.header.src1 <= R7)
					{
						src1 = R[instruction.header.src1];
						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							src2 = R[instruction.header.src2];

							R[0] = src1 / src2;
							R[1] = src1 % src2;

							cout << endl << "R[0] = " << R[0] << " R[1] = " << R[1] << endl;

							if (mul_result == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}
						}
						else if (instruction.header.src2 == ADDR)
						{
							R[0] = R[0] / load(IP);
							R[1] = R[1] % load(IP);
						}	//IP = IP + 2;
					}

					break;

				case CMP:
					cout << endl << " CMP instruction" << endl;
					if (instruction.header.src1 >= R0 && instruction.header.src1 <= R7)
					{
						src1 = R[instruction.header.src1];

						if (instruction.header.src2 >= R0 && instruction.header.src2 <= R7)
						{
							src2 = R[instruction.header.src2];

							if (src1 == src2)
							{
								FLAGS[E] = 1;
								cout << "FLAGS[E] = " << FLAGS[E] << endl;
								//IP = IP + 2;
							}
							else
							{
								FLAGS[E] = 0;
								cout << "FLAGS[E] = " << FLAGS[E] << endl;
							}

							if (src1 == 0 && src2 == 0)
							{
								FLAGS[Z] = 1;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
								//IP = IP + 2;
							}
							else
							{
								FLAGS[Z] = 0;
								cout << "FLAGS[Z] = " << FLAGS[Z] << endl;
							}

							if (src1 > src2)
							{
								FLAGS[G] = 1;
								cout << "FLAGS[G] = " << FLAGS[G] << endl;
								//IP = IP + 2;
							}
							else
							{
								FLAGS[G] = 0;
								cout << "FLAGS[G] = " << FLAGS[G] << endl;
							}
						}
					}
					break;

				case JMP:
					cout << endl << " JMP instruction" << endl;
					IP = instruction.header.src1;
					//IP = IP + 2;
					//IP = IP + 2 * instruction.size;
					EXtoCB.push(IP);
					cout << "EX sends to LS: " << IP << endl;
					break;

				case JE:
					cout << endl << " JE instruction" << endl;
					if (FLAGS[E])
					{
						IP = instruction.header.src1;
						EXtoCB.push(IP);
						cout << "EX sends to LS: " << IP << endl;
					}
					//else
						//IP = IP + 2;
					break;

				case JL:
					cout << endl << " JL instruction" << endl;
					if (!FLAGS[E] && !FLAGS[G])
					{
						IP = instruction.header.src1;
						EXtoCB.push(IP);
						cout << "EX sends to LS: " << IP << endl;
					}
					//else
						//IP = IP + 2;
					break;

				case JG:
					cout << endl << " JG instruction" << endl;
					if (FLAGS[G])
					{
						IP = instruction.header.src1;
						EXtoCB.push(IP);
						cout << "EX sends to LS: " << IP << endl;
					}
					//else
						//IP = IP + 2;
					break;

				case JZ:
					cout << endl << " JZ instruction" << endl;
					if (FLAGS[Z])
					{
						IP = instruction.header.src1;
						EXtoCB.push(IP);
						cout << "EX sends to LS: " << IP << endl;
					}
					//else
						//IP = IP + 2;
					break;

				case CALL:
					cout << endl << " CALL instruction" << endl;
					break;

				case RET:
					cout << endl << " RET instruction" << endl;
					break;

				case END_SIM:
					cout << endl << " END_SIM instruction" << endl;
					break;

				case POP:
					cout << endl << " POP instruction" << endl;
					break;

				case PUSH:
					cout << endl << " PUSH instruction" << endl;
					break;
				default:
					string message = "DEFAULT (EX)!";
					ptrLog->print_m(message);
					//break;
				}

				EXtoCB.push(IP);
				/*IP = IP + 2;
				if (instruction.size == 2)
				{
					IP = IP + 2;
					cout << " p1 is here!" << endl;
				}
				if (instruction.size == 3)
				{
					IP = IP + 2;
					cout << " p2 is here!" << endl;
				}*/
				//done = true;
				newIP = true;
			}
		}
	}
}

void cpu::CompletionBuffer()
{	//aici  VERIFIC
	uint16_t ip;
	uint16_t valoare;
	//vector<uint16_t> v;
	//uint16_t size = IP.size();
	while (run)
	{ 
		
	//-----------
		ip = IP & ~7;
		if ((IP >= ip) && !(IP < ip + 8))
		{
			//update
			//update(valoare, ip);

		}
		if ((IP < ip) && (IP < ip + 8))
		{
			//drop
			string message1 = "DROP!";
			ptrLog->print_m(message1);
		}

		IP = IP + 2;
		if (instruction.header.src1 == ADDR && (instruction.header.src2 >= R0 && instruction.header.src2 <= R7))
		{
			//store(src1, R[src2]);
			IP = IP + 2;
		}
		if ((instruction.header.src1 >= R0 && instruction.header.src1 <= R7) && instruction.header.src2 == ADDR)
		{
			//R[src1] = load(src2);
			IP = IP + 2;
		}
		if (instruction.header.src1 == ADDR && instruction.header.src2 == ADDR)
		{
			//store(src1, load(src2));
			IP = IP + 4;
		}
			

		//if ((IP >= ip) && (IP < ip + 8)){}
		

	}
}
void cpu::update(uint16_t valoare, uint16_t dest)
{
	vector<uint16_t> v;
	//update 
	for (int i = 0; i <= max_ex; i++)
	{
		i = EXtoCB.front();
		EXtoCB.pop();
		string message2 = "IP is: ";
		ptrLog->print_m(message2);
		v.push_back(i);
	}
	dest = valoare;
}

void cpu::LS() {
	uint64_t address;
	uint64_t data;
	//uint16_t address; 
	//mutex mt;
		
	while (run) {
				
		uint64_t curentCycle = cycle;
		while (cycle < curentCycle + d_LS) {
			this_thread::sleep_for(chrono::milliseconds(p_th));
		}

		if (!ICtoLS.empty())
		{
			address = static_cast<uint16_t>(ICtoLS.front());
			ICtoLS.pop();

			string caller_r = "LS received IP from IC ";
			ptrLog->print_ip(caller_r, IP);

			//data = mem.read_memory<uint64_t>(data);
			p.window.val64 = mem->read_memory<uint64_t>(address); 

			LStoIC.push(p);//val64
			string caller_s = "LS sends data to IC ";
			ptrLog->print(caller_s, p.window.val64, p.IP);

			/*if (!CBtoLS.empty())
			{
				data = static_cast<uint16_t>(CBtoLS.front());
				CBtoLS.pop();

				string caller_r = "LS received data from CB ";
				ptrLog->print(caller_r, data, IP);

				//data = mem.read_memory<uint64_t>(data);
				//p.window.val64 = mem->read_memory<uint64_t>(data); 

				/*LStoIC.push_t(w.val64);
				string caller_s = "LS sends data to IC ";
				ptrLog->print(caller_s, w.val64);
				
			}*/
		}
	}
}


void logger::print_ip(string caller, uint16_t message) {

	lock_guard<mutex>lock(mt);
	cout << endl << "clk " << cycle << " " << " message: " << caller<< " " << "IP: " << message<< endl;
}

void logger::print(string caller, uint64_t message, uint16_t ip) {

	lock_guard<mutex>lock(mt);
	cout << endl << "clk " << cycle << " " << " message: " << caller << " " << "data: " << message <<" adresa: "<<ip<< endl;
}

void logger::print_instr_DE( struct instr& instruction){
	
	lock_guard<mutex>lock(mt);
	cout << endl << "clk " << cycle << " DE sends to EX the instruction: "<< hex <<  " opcode: " << instruction.header.opc << " src1: " << instruction.header.src1 << " src2: " << instruction.header.src2 << endl;
}

void logger::print_instr_EX(struct instr& instruction, uint16_t IP) {

	lock_guard<mutex>lock(mt);
	cout << endl << "clk " << cycle << " EX received from DE the instruction: " << hex <<" opcode: " << instruction.header.opc << " src1: " << instruction.header.src1 << " src2: " << instruction.header.src2 <<" IP: "<<IP;
}

void logger::print_m(string message) {

	lock_guard<mutex>lock(mt);
	cout << endl << "clk " << cycle <<" message: " << message << endl;
}
